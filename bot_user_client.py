#!/usr/bin/env python

import asyncio
import json
from multiprocessing import Process
import time
import websockets
import os

from bbb_client import BbbClient

async def hello(uri):
    b = BbbClient()
    try:
        async with websockets.connect(uri) as websocket:
            await websocket.send(json.dumps({'action': 'type', 'type': 'bot'}))
            async for message in websocket:
                try:
                    print(message)
                    data = json.loads(message)
                    if data['action'].startswith('join_'):
                        wait_time = data.get("wait", 0)
                        time.sleep(wait_time)
                        b.start_browser()
                        b.join_bbb(data['url'])
                        if data['action'] == 'join_microphone':
                            b.audio_mic()
                            b.mute()
                        if data['action'] == 'join_listen':
                            b.audio_listen()
                    elif data['action'] == 'teardown':
                        b.stop_browser()
                    elif data['action'] == 'chat':
                        wait_time = data.get("wait", 0)
                        time.sleep(wait_time)
                        b.chat(data.get('message', 'message missing'))
                    elif data['action'] == 'quit':
                        b.stop_browser()
                        return
                    elif data['action'] == 'start_camera':
                        b.start_camera()
                    elif data['action'] == 'stop_camera':
                        b.stop_camera()
                except Exception as e:
                    print(e)
                    raise
    except Exception as e:
        print(e)
    finally:
        b.stop_browser()

def run(uri):
    asyncio.get_event_loop().run_until_complete(hello(uri))

if __name__ == '__main__':
    with open("client.config.json", "rb") as f:
        config = json.loads(f.read())
    os.environ['LANG'] = 'en_US.UTF-8'
    for i in range(config['instances']):
        p = Process(target=run, args=(config['uri'],))
        p.start()
