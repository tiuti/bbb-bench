import asyncio
import json
import logging
import random
import websockets

from bigbluebutton_api_python.core import ApiMethod
from bigbluebutton_api_python.util import UrlBuilder

logging.basicConfig()

class BotServer(object):
    bots = set()
    bot_names = {}
    bots_active = set()
    bots_listen = set()
    bots_microphone = set()
    bots_camera_active = set()
    users = set()
    UNKNOWN=1
    BOT=2
    USER=3

    def __init__(self, apiurl, secret):
        self.apiurl = apiurl
        self.secret = secret

    async def notify_users(self):
        message = json.dumps({'users': len(self.users), 'bots': len(self.bots)})
        print("notify users: %s" % message)
        if self.users:
            await asyncio.wait({user.send(message) for user in self.users})

    async def handle_unknown(self, websocket, data):
        client_type=self.UNKNOWN
        if data["action"] == "type":
            if data["type"] == "bot":
                client_type = self.BOT
                self.bots.add(websocket)
            elif data["type"] == "user":
                client_type = self.USER
                self.users.add(websocket)
            await self.notify_users()
        return client_type

    async def handle_bot(self, websocket, data):
        pass

    async def handle_user(self, websocket, data):
        print("user: %s" % str(data))
        if data["action"] == "join":
            await self.join_bots_to_meeting(data)
        elif data["action"] == "teardown":
            await self.teardown_bots()
        elif data["action"] == "chat":
            await self.chat_bots(data)
        elif data["action"] == "start_camera":
            await self.start_camera(data)
        elif data["action"] == "stop_camera":
            await self.stop_camera(data)
        elif data["action"] == "quit":
            await self.quit_bots()
        pass

    async def join_bots_to_meeting(self, data):
        meetingID = data['meetingID']
        password = data['password']
        wait_time = data.get('wait_time', 5)
        microphone_percent = data.get('microphone_percent', 0.0)
        botlist = list(self.bots)
        random.shuffle(botlist)
        numbots = len(botlist)
        if botlist:
            await asyncio.wait({
                asyncio.ensure_future(bot.send(json.dumps({
                    'action': {True: "join_microphone", False: "join_listen"}[i+1 <= (numbots * microphone_percent/100)],
                    'url': self.join_url(meetingID, f"Bot {i}",  password),
                    'wait': i * wait_time,
                    'numbots': numbots,
                }))) 
                for i, bot in enumerate(botlist)
            })

    async def teardown_bots(self):
        if self.bots:
            await asyncio.wait({
                asyncio.ensure_future(bot.send(json.dumps({
                    'action': 'teardown'
                })))
                for bot in self.bots
            })

    async def quit_bots(self):
        if self.bots:
            await asyncio.wait({
                asyncio.ensure_future(bot.send(json.dumps({
                    'action': 'quit'
                })))
                for bot in self.bots
            })

    async def chat_bots(self, data):
        if self.bots:
            message = data.get("message", "foo bar baz")
            wait_time = data.get('wait_time', 5)
            microphone_percent = data.get('microphone_percent', 0.0)
            await asyncio.wait({
                asyncio.ensure_future(bot.send(json.dumps({
                    'action': 'chat',
                    'message': message,
                    'wait_time': i * wait_time,
                })))
                for i, bot in enumerate(self.bots)
            })

    async def start_camera(self, data):
        if self.bots:
            wait_time = data.get('wait_time', 5)
            await asyncio.wait({
                asyncio.ensure_future(bot.send(json.dumps({
                    'action': 'start_camera',
                    'wait_time': i * wait_time,
                })))
                for i, bot in enumerate(self.bots)
            })
            self.bots_camera_active.update(self.bots)

    async def stop_camera(self, data):
        if self.bots_camera_active:
            wait_time = data.get('wait_time', 5)
            await asyncio.wait({
                asyncio.ensure_future(bot.send(json.dumps({
                    'action': 'stop_camera',
                    'wait_time': i * wait_time,
                })))
                for i, bot in enumerate(self.bots_camera_active)
            })
            self.bots_camera_active = set()

    def join_url(self, meetingID, name, password):
        urlbuilder = UrlBuilder(self.apiurl, self.secret)
        params = {
            'meetingID': meetingID,
            'fullName': name,
            'password': password,
            'joinViaHtml5': True,
        }
        return urlbuilder.buildUrl(ApiMethod.JOIN, params=params)

    def create_meeting(self, meetingID):
        urlbuilder = UrlBuilder(self.apiurl, self.secret)
        params = {
            'meetingID': meetingID,
            'attendeePW': 'ap',
            'moderatorPW': 'mp',
        }
        return urlbuilder.buildUrl(ApiMethod.JOIN, params=params)
        

    async def new_connection(self, websocket, path):
        client_type = self.UNKNOWN
        try:
            async for message in websocket:
                print("%s received %s" % (client_type, message))
                data = json.loads(message)
                if client_type == self.UNKNOWN:
                    client_type = await self.handle_unknown(websocket, data)
                elif client_type == self.BOT:
                    await self.handle_bot(websocket, data)
                elif client_type == self.USER:
                    await self.handle_user(websocket, data)
        except websockets.exceptions.ConnectionClosedError as e:
            logging.debug("%s disconnected" % client_type)
        except:
            logging.exception("Something weird happened")
        finally:
            await self.unregister(websocket)

    async def unregister(self, websocket):
        for s in (self.bots, 
                  self.users,
                  self.bots_active,
                  self.bots_listen,
                  self.bots_camera_active,
                 ):
            if websocket in s:
                s.remove(websocket)
        await self.notify_users()

    async def register(self, websocket):
        self.connections.add(websocket)
        await self.notify_users()

with open("server.config.json", "rb") as f:
    config = json.loads(f.read())

bs = BotServer(config['api'], config['secret'])
start_server = websockets.serve(bs.new_connection, "localhost", 6789)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
